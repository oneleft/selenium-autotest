# filename: headless.rb
require 'bundler/setup'
require 'watir-webdriver'
require 'webdriver-user-agent'
 
driver = Webdriver::UserAgent.driver(:agent => :random)
browser = Watir::Browser.new driver

profile = Selenium::WebDriver::Firefox::Profile.new
profile['network.proxy.socks'] = "127.0.0.1"
profile['network.proxy.socks_port'] = 9050
profile['network.proxy.type'] = 1
random_user_agent_string = File.readlines('useragents.txt').sample
puts random_user_agent_string
profile['general.useragent.override'] = random_user_agent_string
$browser = Watir::Browser.new :firefox, :profile => profile

$browser.goto "claimbtc.com" 
$browser.screenshot.save 'output/headless'+ARGV[0]+'.png'