# Resources for this project

This is a list of all resources used while creating this project

* __Initial setup *xvfb*__ [http://elementalselenium.com/tips/38-headless](http://elementalselenium.com/tips/38-headless)
* __Add to shippable config *Selenium web-driver*__ [http://stackoverflow.com/questions/22952022/ruby-selenium-cannot-load-such-file-selenium-webdriver-loaderror](http://stackoverflow.com/questions/22952022/ruby-selenium-cannot-load-such-file-selenium-webdriver-loaderror)
* __Add to shippable config *Ruby gems*__ [http://ask.xmodulo.com/install-rubygems-linux.html](http://ask.xmodulo.com/install-rubygems-linux.html)
* __Modified *Ruby Gems*__ [http://stackoverflow.com/questions/23985887/install-error-rubygems-has-no-installation-candidate](http://stackoverflow.com/questions/23985887/install-error-rubygems-has-no-installation-candidate)
* __Read command line *Ruby*__ [http://stackoverflow.com/questions/4244611/pass-variables-to-ruby-script-via-command-line](http://stackoverflow.com/questions/4244611/pass-variables-to-ruby-script-via-command-line)
* __Selenium and Tor setup *TOR*__ [http://lyle.smu.edu/~jwadleigh/seltest/](http://lyle.smu.edu/~jwadleigh/seltest/)
* __Ubuntu wiki *TOR*__ [http://ubuntuguide.org/wiki/Tor](http://ubuntuguide.org/wiki/Tor)
* __Shell background service *TOR*__ [http://tor.stackexchange.com/questions/4587/run-tor-in-the-background-via-the-shell](http://tor.stackexchange.com/questions/4587/run-tor-in-the-background-via-the-shell)
* __User Agents list *Firefox*__[https://github.com/wpscanteam/CMSScanner/blob/master/app/user_agents.txt](https://github.com/wpscanteam/CMSScanner/blob/master/app/user_agents.txt)
* __Bash Command Line Examples *For Loop*__ [http://www.bashoneliners.com/](http://www.bashoneliners.com/)
* __Exit Nodes *TOR*__ [http://tor.stackexchange.com/questions/733/can-i-exit-from-a-specific-country-or-node](http://tor.stackexchange.com/questions/733/can-i-exit-from-a-specific-country-or-node)
* __Get new identity *TOR*__ [https://github.com/craig/ge.mine.nu/blob/master/lbd/tor-ctrl.sh](https://github.com/craig/ge.mine.nu/blob/master/lbd/tor-ctrl.sh)